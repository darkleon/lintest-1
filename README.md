[![pipeline status](https://gitlab.com/darkleon/lintest-1/badges/master/pipeline.svg)](https://gitlab.com/darkleon/lintest-1/commits/master)

FROM scratch
COPY hello /
CMD ["/hello"]